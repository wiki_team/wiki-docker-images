# DockerHubにあるAlpine LinuxのPython環境を元にする
FROM python:alpine

# MkDocsに含まれるregexをビルドするためのgccがないのでインストール
RUN apk add build-base

# mkdocsをインストール 
RUN pip install --upgrade pip \
  && pip install mkdocs \
  && pip install mkdocs-material

# ローカルでテストするための設定
WORKDIR /root
CMD ["mkdocs", "serve", "-a", "0.0.0.0:8000"]
